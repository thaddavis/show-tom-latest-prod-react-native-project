package com.example.oceanview;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class OceanviewHelper {
    private static final String TAG = "OceanviewHelper";
    public static final String VERSION = "2.0.14.0";

    public OceanviewHelper() {}

    public static void startOV(Activity activity, String jsonStr) {
        final Intent ovIntent = new Intent(activity, OceanviewMainActivity.class);

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            ovIntent.putExtra("access_token", jsonObj.getString("access_token"));
            ovIntent.putExtra("userid", jsonObj.getString("userid"));
            ovIntent.putExtra("refresh_token", jsonObj.getString("refresh_token"));
            ovIntent.putExtra("expiry_in", jsonObj.getString("expiry_in"));
            ovIntent.putExtra("env", jsonObj.getString("env"));
            ovIntent.putExtra("allowExit", jsonObj.getBoolean("allowExit"));
            ovIntent.putExtra("contentId", jsonObj.getString("contentId"));
            ovIntent.putExtra("enableControls", jsonObj.getBoolean("enableControls"));
            ovIntent.putExtra("generateAnalytics", jsonObj.getBoolean("generateAnalytics"));
            ovIntent.putExtra("guestId", jsonObj.getString("guestId"));
            ovIntent.putExtra("persistPosition", jsonObj.getBoolean("persistPosition"));
            ovIntent.putExtra("resumeAllowed", jsonObj.getBoolean("resumeAllowed"));
            ovIntent.putExtra("returnData", jsonObj.getString("returnData"));
            ovIntent.putExtra("startPosition", jsonObj.getInt("startPosition"));
            ovIntent.putExtra("stayInOceanView", jsonObj.getBoolean("stayInOceanView"));
            ovIntent.putExtra("subtitles", jsonObj.getBoolean("subtitles"));
            ovIntent.putExtra("subtitlesLang", jsonObj.getString("subtitlesLang"));
            ovIntent.putExtra("useDetailPageOnEntry", jsonObj.getBoolean("useDetailPageOnEntry"));
            ovIntent.putExtra("userDetailPageOnExit", jsonObj.getBoolean("userDetailPageOnExit"));
            ovIntent.putExtra("bundleType", "framework");

            activity.startActivity(ovIntent);
        } catch (final JSONException e) {
            System.out.println("Error parsing params passed from MClass");
            System.out.println(e.toString());
        }
    }

    public static String getVersion(){
        return VERSION;
    }

    private static UnityCallback unityCallback;

    public static void setUnityCallBackListener(UnityCallback callback) {
        unityCallback = callback;
    }

    public static UnityCallback getUnityCallBackListener() {
        return unityCallback;
    }
}
