package com.example.oceanview;

public interface UnityCallback {
    public void delegateMessageReceived(String data);
}