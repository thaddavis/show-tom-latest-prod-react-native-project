package com.example.oceanview;

import android.app.Activity;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

import static com.example.oceanview.OceanviewHelper.getUnityCallBackListener;

public class OceanviewBridgeModule extends ReactContextBaseJavaModule {
    private static final String TAG = "OceanviewBridgeModule";
    private static ReactApplicationContext reactContext;

    private ReactApplicationContext reactApplicationContext;

    public OceanviewBridgeModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "OceanviewBridgeModule";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        return constants;
    }

    @ReactMethod
    public void returnSafetyVideoData(String message) {
        try {
            Log.d(TAG, "returnSafetyVideoData: YEEE!!!" + message);
            UnityCallback callback = getUnityCallBackListener();
            callback.delegateMessageReceived(message);
        } catch(Exception e) {
            Log.d(TAG, "returnSafetyVideoData: Failed" +e.toString());
        }
    }

    @ReactMethod
    public void quitOceanview() {
        Activity currentActivity = getCurrentActivity();
        currentActivity.finish();
    }

}