package com.example.exoplayer.ui;

import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;

import com.example.exoplayer.R;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
//import com.oceanview.R;

import java.util.Locale;

/**
 * This class is a wrapper for DefaultTrackNameProvider to override the properties
 */
public class OVDefaultTrackNameProvider extends DefaultTrackNameProvider {

    private final Resources ovResources;

    /**
     * @param resources Resources from which to obtain strings.
     */
    public OVDefaultTrackNameProvider(Resources resources) {
        super(resources);
        this.ovResources = Assertions.checkNotNull(resources);
    }

    @Override
    public String getTrackName(Format format) {
        // Log.d("TRACKS!!!", "___ ___ ___");
        System.out.println("TRACKS!!!");

        String trackName;
        int trackType = inferPrimaryTrackType(format);
        if (trackType == C.TRACK_TYPE_VIDEO
                /*
                ||
                trackType == C.TRACK_TYPE_AUDIO
                */
        ) {
            trackName = super.getTrackName(format);
        } else {
            trackName = buildLanguageOrLabelString(format);
        }
        return trackName.length() == 0 ? ovResources.getString(R.string.exo_track_unknown) : trackName;
    }

    private String buildLanguageOrLabelString(Format format) {
        String languageAndRole =
                joinWithSeparator(buildLanguageString(format), buildRoleString(format));
        return TextUtils.isEmpty(languageAndRole) ? buildLabelString(format) : languageAndRole;
    }

    private String buildRoleString(Format format) {
        String roles = "";
        if ((format.roleFlags & C.ROLE_FLAG_ALTERNATE) != 0) {
            roles = ovResources.getString(R.string.exo_track_role_alternate);
        }
        if ((format.roleFlags & C.ROLE_FLAG_SUPPLEMENTARY) != 0) {
            roles = joinWithSeparator(roles, ovResources.getString(R.string.exo_track_role_supplementary));
        }
        if ((format.roleFlags & C.ROLE_FLAG_COMMENTARY) != 0) {
            roles = joinWithSeparator(roles, ovResources.getString(R.string.exo_track_role_commentary));
        }
        if ((format.roleFlags & (C.ROLE_FLAG_CAPTION | C.ROLE_FLAG_DESCRIBES_MUSIC_AND_SOUND)) != 0) {
            roles =
                    joinWithSeparator(roles, ovResources.getString(R.string.exo_track_role_closed_captions));
        }
        return roles;
    }

    private String joinWithSeparator(String... items) {
        String itemList = "";
        for (String item : items) {
            if (item.length() > 0) {
                if (TextUtils.isEmpty(itemList)) {
                    itemList = item;
                } else {
                    itemList = ovResources.getString(R.string.exo_item_list, itemList, item);
                }
            }
        }
        return itemList;
    }

    private String buildLabelString(Format format) {
        return TextUtils.isEmpty(format.label) ? "" : format.label;
    }

    private String buildLanguageString(Format format) {
        String language = format.language;
        if (TextUtils.isEmpty(language) || C.LANGUAGE_UNDETERMINED.equals(language)) {
            return "";
        }
        Locale locale = Util.SDK_INT >= 21 ? Locale.forLanguageTag(language) : new Locale(language);
        return locale.getDisplayName().isEmpty()? format.language : locale.getDisplayName();
    }

    private static int inferPrimaryTrackType(Format format) {
        int trackType = MimeTypes.getTrackType(format.sampleMimeType);
        if (trackType != C.TRACK_TYPE_UNKNOWN) {
            return trackType;
        }
        if (MimeTypes.getVideoMediaMimeType(format.codecs) != null) {
            return C.TRACK_TYPE_VIDEO;
        }
        if (MimeTypes.getAudioMediaMimeType(format.codecs) != null) {
            return C.TRACK_TYPE_AUDIO;
        }
        if (format.width != Format.NO_VALUE || format.height != Format.NO_VALUE) {
            return C.TRACK_TYPE_VIDEO;
        }
        if (format.channelCount != Format.NO_VALUE || format.sampleRate != Format.NO_VALUE) {
            return C.TRACK_TYPE_AUDIO;
        }
        return C.TRACK_TYPE_UNKNOWN;
    }
}
