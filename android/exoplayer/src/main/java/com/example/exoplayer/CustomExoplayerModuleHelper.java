package com.example.exoplayer;

import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;

import java.util.HashMap;

public class CustomExoplayerModuleHelper {

    public static HashMap<String, String> ExtractSubtitles(ReadableMap subtitles) {
        ReadableMapKeySetIterator itr = subtitles.keySetIterator();
        HashMap<String, String> subtitlesHashMap = new HashMap<String, String>();
        while (itr.hasNextKey()) {
            String K = itr.nextKey().toString();
            String V = subtitles.getString(K);
            subtitlesHashMap.put(K, V);
        }

        return subtitlesHashMap;
    }
}