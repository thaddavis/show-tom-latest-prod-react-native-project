/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.exoplayer;

import static com.example.exoplayer.IntentUtil.PLAYLIST_START_INDEX;
import static com.google.android.exoplayer2.util.Assertions.checkNotNull;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
// import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;

import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
//import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.StyledPlayerControlView;
//import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.ui.PlayerView;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

 import com.example.exoplayer.R;

/** An activity that plays media using {@link SimpleExoPlayer}. */
public class PlayerActivity extends AppCompatActivity
    implements OnClickListener, PlaybackPreparer, PlayerControlView.VisibilityListener {

  private static final String TAG = "PlayerActivity";
  
  // Saved instance state keys.

  private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
  private static final String KEY_WINDOW = "window";
  private static final String KEY_POSITION = "position";
  private static final String KEY_AUTO_PLAY = "auto_play";

  private static final CookieManager DEFAULT_COOKIE_MANAGER;

  static {
    DEFAULT_COOKIE_MANAGER = new CookieManager();
    DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
  }

  int SAFETY_VIDEO_MODE = 3;

  public static final String TITLE = "title";
  public static final String VIDEO_ID = "video_id";
  public static final String URI_LIST_EXTRA = "uri_list";
  public static final String TITLE_LIST = "title_list";
  public static final String ID_LIST_EXTRA = "id_list";
  public static final String NUMBER_LIST = "number_list";
  public static final String PROGRESS_IN_SECONDS = "progress_in_seconds";
  public static final String PLAYLIST_START_INDEX = "playlist_start_index";
  public static final String ABR_ALGORITHM_RANDOM = "random";

  public static final String HIDE_CONTROLS = "hide_controls";
  public static final String CONTENT_TYPE = "video";
  public static final String ABR_ALGORITHM_EXTRA = "abr_algorithm";
  public static final String ABR_ALGORITHM_DEFAULT = "default";
  protected PlayerView playerView;

//  protected StyledPlayerView playerView;
  protected LinearLayout debugRootView;
  protected TextView debugTextView;
  protected SimpleExoPlayer player;

  private boolean isShowingTrackSelectionDialog;
  private ImageButton selectTracksButton;
  private DataSource.Factory dataSourceFactory;
  private List<MediaItem> mediaItems;
  private DefaultTrackSelector trackSelector;
  private DefaultTrackSelector.Parameters trackSelectorParameters;
//  private DebugTextViewHelper debugViewHelper;
  private TrackGroupArray lastSeenTrackGroupArray;

  private ImageButton playerLeftArrow;
  private LinearLayout liveTVLayout;
  private FrameLayout movieLayout;
  private ImageButton previousPlaylistItemButton;
  private ImageButton nextPlaylistItemButton;
  private ProgressTracker progressTracker;

  private ImageButton channelUpButton;
  // private ImageButton channelNo;
  private ImageButton channelDownButton;

  private boolean startAutoPlay;
  private int startWindow;
  private long startPosition;
  private String videoId;

  // Fields used only for ad playback.

  private AdsLoader adsLoader;
  private Uri loadedAdTagUri;

  // Activity lifecycle

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    dataSourceFactory = DemoUtil.getDataSourceFactory(/* context= */ this);
    if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
      CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
    }

    setContentView();

    playerView = findViewById(R.id.player_view);
    playerView.setControllerVisibilityListener(this);
    playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
    playerView.requestFocus();

    if (savedInstanceState != null) {
      trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
      startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
      startWindow = savedInstanceState.getInt(KEY_WINDOW);
      startPosition = savedInstanceState.getLong(KEY_POSITION);
    } else {
      DefaultTrackSelector.ParametersBuilder builder =
          new DefaultTrackSelector.ParametersBuilder(/* context= */ this);
      trackSelectorParameters = builder.build();
      clearStartPosition();
    }

    movieLayout = findViewById(R.id.player_buttons);

    movieLayout.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        try {} catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    liveTVLayout = findViewById(R.id.liveTVLayout_buttons);
    String typeOfVideo = getIntent().getStringExtra(CONTENT_TYPE);
    System.out.println("typeOfVideo CONTENT_TYPE" + typeOfVideo);

    if (typeOfVideo.equals("video_stream")) {
      System.out.println("here liveTVLayout");
      liveTVLayout.setVisibility(View.VISIBLE);
      movieLayout.setVisibility(View.GONE);
    } else {
      System.out.println("here movieLayout");
      liveTVLayout.setVisibility(View.GONE);
      movieLayout.setVisibility(View.VISIBLE);
    }

    String videoId = getIntent().getStringExtra(VIDEO_ID);

    playerLeftArrow = findViewById(R.id.player_left_arrow);
    playerLeftArrow.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        try {
          long currentPosition = player.getCurrentPosition();
          int currentPositionInteger = (int) (Math.floor(currentPosition / 1000));
          int mediaDuration = (int) (Math.floor(player.getContentDuration() / 1000));

          WritableMap params = Arguments.createMap();
          params.putString("state", "onBackButton");
          params.putString("videoId", videoId);
          params.putInt("progress", currentPositionInteger);
          params.putInt("duration", mediaDuration);
          CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerProgress", params);

          // For exiting OV when in safety mode
          CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("exitPlayer", Arguments.createMap());

          finish();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    // -------------------------------------------------- Text View that displays
    // video title
    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      TextView titleTextView = (TextView) findViewById(R.id.textView);
      titleTextView.setText(bundle.getString(TITLE));
    }

    // debugRootView = findViewById(R.id.controls_root);
    // debugTextView = findViewById(R.id.debug_text_view);
    selectTracksButton = findViewById(R.id.select_tracks_button);
    selectTracksButton.setOnClickListener(this);

    playerView = findViewById(R.id.player_view);
    playerView.setControllerVisibilityListener(this);
    playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
    playerView.requestFocus();

    if (savedInstanceState != null) {
      trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
      startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
      startWindow = savedInstanceState.getInt(KEY_WINDOW);
      startPosition = savedInstanceState.getLong(KEY_POSITION);
    } else {
      // Out of the BOX OG code
      trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().setPreferredAudioLanguage("en").build();

      clearStartPosition();
    }

    TextView channelNumberTextView = (TextView) findViewById(R.id.channel_number);
    String uriStrings[] = getIntent().getStringArrayExtra(URI_LIST_EXTRA);
    String[] titleStrings = getIntent().getStringArrayExtra(TITLE_LIST);
    String[] idStrings = getIntent().getStringArrayExtra(ID_LIST_EXTRA);
    String[] numberStrings = getIntent().getStringArrayExtra(NUMBER_LIST);

    channelUpButton = (ImageButton) findViewById(R.id.up_chevron);
    channelUpButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        try {
          TextView titleTextView = (TextView) findViewById(R.id.textView);
          Integer nextWindowIndex = player.getNextWindowIndex();

          if (nextWindowIndex < uriStrings.length && nextWindowIndex != -1) {
            player.seekTo(nextWindowIndex, 0);
            titleTextView.setText(titleStrings[nextWindowIndex]);
            String channelId = idStrings[nextWindowIndex];
            channelNumberTextView.setText(numberStrings[nextWindowIndex]);
            System.out.println("here nextWindowIndex " + nextWindowIndex);
            progressTracker.reportProgressForLiveTV("onChannelUp", channelId);
          }

        } catch (Exception e) {
          System.out.println("can't find next window");
          e.printStackTrace();
        }
      }
    });

    channelDownButton = (ImageButton) findViewById(R.id.down_chevron);
    channelDownButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        try {
          System.out.println("here channelDownButton ");
          TextView titleTextView = (TextView) findViewById(R.id.textView);
          Integer previousWindowIndex = player.getPreviousWindowIndex();

          if (previousWindowIndex < 0) {
            previousWindowIndex = 0;
          }

          player.seekTo(previousWindowIndex, 0);
          titleTextView.setText(titleStrings[previousWindowIndex]);
          channelNumberTextView.setText(numberStrings[previousWindowIndex]);
          String channelId = idStrings[previousWindowIndex];
          progressTracker.reportProgressForLiveTV("onChannelDown", channelId);

          System.out.println("here previousWindowIndex " + previousWindowIndex);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

  }

  @Override
  public void onBackPressed() {
    // For exiting OV when in safety mode
    long currentPosition = player.getCurrentPosition();
    int currentPositionInteger = (int) (Math.floor(currentPosition / 1000));
    int mediaDuration = (int) (Math.floor(player.getContentDuration() / 1000));

    WritableMap params = Arguments.createMap();
    params.putString("state", "onBackButton");
    params.putString("videoId", videoId);
    params.putInt("progress", currentPositionInteger);
    params.putInt("duration", mediaDuration);

    CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("exitPlayer", Arguments.createMap());
    super.onBackPressed();
  }

  @Override
  public void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    releasePlayer();
    releaseAdsLoader();
    clearStartPosition();
    setIntent(intent);
  }

  @Override
  public void onStart() {
    super.onStart();
    if (Util.SDK_INT > 23) {
      initializePlayer();
      if (playerView != null) {
        playerView.onResume();
      }
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (Util.SDK_INT <= 23 || player == null) {
      initializePlayer();
      if (playerView != null) {
        playerView.onResume();
      }
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    if (Util.SDK_INT <= 23) {
      if (playerView != null) {
        playerView.onPause();
      }
      releasePlayer();
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    if (Util.SDK_INT > 23) {
      if (playerView != null) {
        playerView.onPause();
      }
      releasePlayer();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    releaseAdsLoader();

    this.progressTracker.cancel();
  }

  @Override
  public void onRequestPermissionsResult(
      int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (grantResults.length == 0) {
      // Empty results are triggered if a permission is requested while another request was already
      // pending and can be safely ignored in this case.
      return;
    }
    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      initializePlayer();
    } else {
      showToast(R.string.storage_permission_denied);
      finish();
    }
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    updateTrackSelectorParameters();
    updateStartPosition();
    outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
    outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
    outState.putInt(KEY_WINDOW, startWindow);
    outState.putLong(KEY_POSITION, startPosition);
  }

  // Activity input

  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {
    // See whether the player view wants to handle media or DPAD keys events.
    return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
  }

  // OnClickListener methods

  @Override
  public void onClick(View view) {
    if (view == selectTracksButton
        && !isShowingTrackSelectionDialog
        && TrackSelectionDialog.willHaveContent(trackSelector)) {
      isShowingTrackSelectionDialog = true;
      TrackSelectionDialog trackSelectionDialog =
          TrackSelectionDialog.createForTrackSelector(
              trackSelector,
              /* onDismissListener= */ dismissedDialog -> isShowingTrackSelectionDialog = false);
      trackSelectionDialog.show(getSupportFragmentManager(), /* tag= */ null);
    }
  }

  // PlaybackPreparer implementation

  @Override
  public void preparePlayback() {
    player.prepare();
  }

  // PlayerControlView.VisibilityListener implementation

  @Override
  public void onVisibilityChange(int visibility) {
    //  debugRootView.setVisibility(visibility);
  }

  // Internal methods

  protected void setContentView() {
    setContentView(R.layout.player_activity);
  }

  /** @return Whether initialization was successful. */
  protected boolean initializePlayer() {
    if (player == null) {
      Intent intent = getIntent();
      String action = intent.getAction();

      mediaItems = createMediaItems(intent);
      if (mediaItems.isEmpty()) {
        return false;
      }
      LinearLayout bottom_group = playerView.findViewById(R.id.exo_bottom_group);

      String enableControls = getIntent().getStringExtra(HIDE_CONTROLS);

      if (enableControls.equals("YES")) {
        bottom_group.setVisibility(View.VISIBLE);
        movieLayout.setVisibility(View.VISIBLE);
      } else {
        bottom_group.setVisibility(View.GONE);
        movieLayout.setVisibility(View.GONE);
      }

      String typeOfVideo = getIntent().getStringExtra(CONTENT_TYPE);
      System.out.println("here 1 "+typeOfVideo);
      if (IntentUtil.ACTION_VIEW.equals(action) && typeOfVideo.equals("video_stream")) {
        System.out.println("here 2 "+typeOfVideo);

        // LIVE TV MEDIA MODE
        movieLayout.setVisibility(View.GONE);
        bottom_group.setVisibility(View.GONE);

        Uri[] uris;
        MergingMediaSource[] concatenatedMergedMediaSourcesForPlaylistMode;
        String[] uriStrings = intent.getStringArrayExtra(URI_LIST_EXTRA);
        String[] idStrings = intent.getStringArrayExtra(ID_LIST_EXTRA);
        System.out.println("here 3 "+uriStrings);
        System.out.println("here 4 "+idStrings);

        uris = new Uri[uriStrings.length];
        for (int i = 0; i < uriStrings.length; i++) {
          try {
            uris[i] = Uri.parse(uriStrings[i]);
          } catch (Exception e) {
            System.out.println("PARSE_MEDIA_URL_ERROR" + e.toString());
          }
        }

        String[] titleStrings = intent.getStringArrayExtra(TITLE_LIST);
        TextView titleTextView = (TextView) findViewById(R.id.textView);

        String[] numberStrings = intent.getStringArrayExtra(NUMBER_LIST);
        TextView channelNumberTextView = (TextView) findViewById(R.id.channel_number);

        System.out.println("here 5 "+uris);
        System.out.println("here 6 "+titleStrings);
        System.out.println("here 7 "+numberStrings);

        boolean preferExtensionDecoders = intent.getBooleanExtra(IntentUtil.PREFER_EXTENSION_DECODERS_EXTRA, false);
        RenderersFactory renderersFactory = DemoUtil.buildRenderersFactory(/* context= */ this, preferExtensionDecoders);
        TrackSelection.Factory trackSelectionFactory;
        String abrAlgorithm = intent.getStringExtra(ABR_ALGORITHM_EXTRA);
        if (abrAlgorithm == null || ABR_ALGORITHM_DEFAULT.equals(abrAlgorithm)) {
          trackSelectionFactory = new AdaptiveTrackSelection.Factory();
        } else if (ABR_ALGORITHM_RANDOM.equals(abrAlgorithm)) {
          trackSelectionFactory = new RandomTrackSelection.Factory();
        } else {
//          showToast(R.string.error_unrecognized_abr_algorithm);
          finish();
//          return;
        }

        trackSelector = new DefaultTrackSelector(this);
        trackSelector.setParameters(trackSelectorParameters);
        lastSeenTrackGroupArray = null;
        MediaSourceFactory mediaSourceFactory =
                new DefaultMediaSourceFactory(dataSourceFactory);

        player = new SimpleExoPlayer.Builder(/* context= */ this, renderersFactory)
//                .setTrackSelector(trackSelector)
                .setMediaSourceFactory(mediaSourceFactory)
                .build();

        System.out.println("here 71 "+player);

        this.progressTracker = new ProgressTracker(player, videoId, true);
        String[] progressTrackerVideoIds = idStrings;

        // Listener For LiveTV
        player.addListener(new PlayerEventListener(player, videoId, true));

        // Narendra
        player.setPlayWhenReady(startAutoPlay);
        player.addAnalyticsListener(new EventLogger(trackSelector));
        playerView.setPlayer(player);
        playerView.setPlaybackPreparer(this);

        MediaSource[] mediaSources = new MediaSource[uris.length];
        for (int i = 0; i < uris.length; i++) {
          mediaSources[i] = buildMediaSource(uris[i], null);
        }

        MediaSource mediaSource;
        System.out.println("here 8 " + mediaSources);
        mediaSource = new ConcatenatingMediaSource(mediaSources);
        startPosition = (long) intent.getIntExtra(PlayerActivity.PROGRESS_IN_SECONDS, 0) * 1000;
        System.out.println("here 9 " + startPosition);

        startWindow = intent.getIntExtra(PLAYLIST_START_INDEX, 0);
        titleTextView.setText(titleStrings[startWindow]);
        channelNumberTextView.setText(numberStrings[startWindow]);

        System.out.println("here 10 " + startWindow);
        player.seekTo(startWindow, startPosition);
        // this.progressTracker.reportProgress("began");

        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        System.out.println("here 11 " + mediaSource);
        //narendra
        player.setMediaSource(mediaSource);
        player.prepare();
        player.play();
//        player.prepare(mediaSource, !haveStartPosition, false);
        System.out.println("here 12 " + startWindow);

        /*

        * */
        updateButtonVisibility();
        return true;
      }else {


        boolean preferExtensionDecoders =
            intent.getBooleanExtra(IntentUtil.PREFER_EXTENSION_DECODERS_EXTRA, false);
        RenderersFactory renderersFactory =
            DemoUtil.buildRenderersFactory(/* context= */ this, preferExtensionDecoders);
        MediaSourceFactory mediaSourceFactory =
            new DefaultMediaSourceFactory(dataSourceFactory);
  //              .setAdsLoaderProvider(this::getAdsLoader)
  //              .setAdViewProvider(playerView);

        trackSelector = new DefaultTrackSelector(/* context= */ this);
        trackSelector.setParameters(trackSelectorParameters);
        lastSeenTrackGroupArray = null;
        System.out.println("here 21");
        player =
            new SimpleExoPlayer.Builder(/* context= */ this, renderersFactory)
                .setMediaSourceFactory(mediaSourceFactory)
                .setTrackSelector(trackSelector)
                .build();
        this.videoId = intent.getStringExtra(VIDEO_ID);
        this.progressTracker = new ProgressTracker(player, videoId, false);
        player.addListener(new PlayerEventListener(player, videoId, false));
        player.addAnalyticsListener(new EventLogger(trackSelector));
        player.setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true);
        player.setPlayWhenReady(startAutoPlay);
        playerView.setPlayer(player);
        playerView.setPlaybackPreparer(this);
  //      debugViewHelper = new DebugTextViewHelper(player, debugTextView);
  //      debugViewHelper.start();

          boolean haveStartPosition = startWindow != C.INDEX_UNSET;
          if (haveStartPosition) {
            player.seekTo(startWindow, startPosition);
          }
          player.setMediaItems(mediaItems, /* resetPosition= */ !haveStartPosition);
          player.prepare();
          updateButtonVisibility();
        }
    }

    System.out.println("here 100");

    Bundle bundle = getIntent().getExtras();
    startPosition = (long) bundle.getInt(PlayerActivity.PROGRESS_IN_SECONDS, 0) * 1000;
    startWindow = (int) bundle.getInt(PLAYLIST_START_INDEX, 0);

    boolean haveStartPosition = startWindow != C.INDEX_UNSET;
    if (haveStartPosition) {
      player.seekTo(startWindow, startPosition);
    }
    player.setMediaItems(mediaItems, /* resetPosition= */ !haveStartPosition);
    player.prepare();
    System.out.println("here 101");

    updateButtonVisibility();
    return true;
  }

  private MediaSource buildMediaSource(Uri uri) {
    return buildMediaSource(uri, null);
  }

  private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
    System.out.println("here 16 "+uri);

    DownloadRequest downloadRequest = DemoUtil.getDownloadTracker(this).getDownloadRequest(uri);
    if (downloadRequest != null) {
      return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory);
    }
    @C.ContentType
    int type = Util.inferContentType(uri, overrideExtension);
    System.out.println("here 17 "+type);

    switch (type) {
      case C.TYPE_DASH:
        System.out.println("here 12");
        return new DashMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
      case C.TYPE_SS:
        return new SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
      case C.TYPE_HLS:
        System.out.println("here 13");

        return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
      case C.TYPE_OTHER:
        System.out.println("here 14");

        return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
      default:
        System.out.print("here 15");

        throw new IllegalStateException("Unsupported type: " + type);
    }
  }

  private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension, String tag) {
    System.out.println("here 116 "+uri);

    DownloadRequest downloadRequest = DemoUtil.getDownloadTracker(this).getDownloadRequest(uri);
    if (downloadRequest != null) {
      return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory);
    }
    @C.ContentType
    int type = Util.inferContentType(uri, overrideExtension);
    switch (type) {
      case C.TYPE_DASH:
        return new DashMediaSource.Factory(dataSourceFactory).setTag(tag).createMediaSource(uri);
      case C.TYPE_SS:
        return new SsMediaSource.Factory(dataSourceFactory).setTag(tag).createMediaSource(uri);
      case C.TYPE_HLS:
        return new HlsMediaSource.Factory(dataSourceFactory).setTag(tag).createMediaSource(uri);
      case C.TYPE_OTHER:
        return new ProgressiveMediaSource.Factory(dataSourceFactory).setTag(tag).createMediaSource(uri);
      default:
        throw new IllegalStateException("Unsupported type: " + type);
    }
  }
  private List<MediaItem> createMediaItems(Intent intent) {
    String action = intent.getAction();
    boolean actionIsListView = IntentUtil.ACTION_VIEW_LIST.equals(action);
    if (!actionIsListView && !IntentUtil.ACTION_VIEW.equals(action)) {
      showToast(getString(R.string.unexpected_intent_action, action));
      finish();
      return Collections.emptyList();
    }

    List<MediaItem> mediaItems =
        createMediaItems(intent, DemoUtil.getDownloadTracker(/* context= */ this));
    boolean hasAds = false;
    for (int i = 0; i < mediaItems.size(); i++) {
      MediaItem mediaItem = mediaItems.get(i);

      if (!Util.checkCleartextTrafficPermitted(mediaItem)) {
        showToast(R.string.error_cleartext_not_permitted);
        return Collections.emptyList();
      }
      if (Util.maybeRequestReadExternalStoragePermission(/* activity= */ this, mediaItem)) {
        // The player will be reinitialized if the permission is granted.
        return Collections.emptyList();
      }

      MediaItem.DrmConfiguration drmConfiguration =
          checkNotNull(mediaItem.playbackProperties).drmConfiguration;
      if (drmConfiguration != null) {
        if (Util.SDK_INT < 18) {
          showToast(R.string.error_drm_unsupported_before_api_18);
          finish();
          return Collections.emptyList();
        } else if (!FrameworkMediaDrm.isCryptoSchemeSupported(drmConfiguration.uuid)) {
          showToast(R.string.error_drm_unsupported_scheme);
          finish();
          return Collections.emptyList();
        }
      }
      hasAds |= mediaItem.playbackProperties.adTagUri != null;
    }
    if (!hasAds) {
      releaseAdsLoader();
    }
    return mediaItems;
  }

//  private AdsLoader getAdsLoader(Uri adTagUri) {
//    if (mediaItems.size() > 1) {
//      showToast(R.string.unsupported_ads_in_playlist);
//      releaseAdsLoader();
//      return null;
//    }
//    if (!adTagUri.equals(loadedAdTagUri)) {
//      releaseAdsLoader();
//      loadedAdTagUri = adTagUri;
//    }
//    // The ads loader is reused for multiple playbacks, so that ad playback can resume.
//    if (adsLoader == null) {
//      adsLoader = new ImaAdsLoader.Builder(/* context= */ this).build();
//    }
//    adsLoader.setPlayer(player);
//    return adsLoader;
//  }

  protected void releasePlayer() {
    if (player != null) {
      updateTrackSelectorParameters();
      updateStartPosition();
//      debugViewHelper.stop();
//      debugViewHelper = null;
      player.release();
      player = null;
      mediaItems = Collections.emptyList();
      trackSelector = null;
    }
    if (adsLoader != null) {
      adsLoader.setPlayer(null);
    }
  }

  private void releaseAdsLoader() {
    if (adsLoader != null) {
      adsLoader.release();
      adsLoader = null;
      loadedAdTagUri = null;
      playerView.getOverlayFrameLayout().removeAllViews();
    }
  }

  private void updateTrackSelectorParameters() {
    if (trackSelector != null) {
      trackSelectorParameters = trackSelector.getParameters();
    }
  }

  private void updateStartPosition() {
    if (player != null) {
      startAutoPlay = player.getPlayWhenReady();
      startWindow = player.getCurrentWindowIndex();
      startPosition = Math.max(0, player.getContentPosition());
    }
  }

  protected void clearStartPosition() {
    startAutoPlay = true;
    startWindow = C.INDEX_UNSET;
    startPosition = C.TIME_UNSET;
  }

  // User controls

  private void updateButtonVisibility() {
    //    selectTracksButton.setEnabled(
    //        player != null && TrackSelectionDialog.willHaveContent(trackSelector));
  }

  private void showControls() {
//    debugRootView.setVisibility(View.VISIBLE);
  }

  private void showToast(int messageId) {
    showToast(getString(messageId));
  }

  private void showToast(String message) {
    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
  }

  private static boolean isBehindLiveWindow(ExoPlaybackException e) {
    if (e.type != ExoPlaybackException.TYPE_SOURCE) {
      return false;
    }
    Throwable cause = e.getSourceException();
    while (cause != null) {
      if (cause instanceof BehindLiveWindowException) {
        return true;
      }
      cause = cause.getCause();
    }
    return false;
  }

  // *****************************************************************
  // ----------------------- PROGRESS TRACKER -----------------------
  // *****************************************************************
  private class ProgressTracker implements Runnable {

    private SimpleExoPlayer player;
    private Handler handler;
    private String videoId;
    private Boolean isLiveTV;
    Boolean stopReportingEvents = false;

    public ProgressTracker(SimpleExoPlayer player, String videoId, Boolean isLiveTV) {

      this.player = player;
      this.videoId = videoId;
      this.isLiveTV = isLiveTV;
      handler = new Handler();
      handler.post(this);
    }

    public void run() {
      int mediaDuration = (int) (Math.floor(this.player.getContentDuration() / 1000));
      int onePercentInterval = (int) (Math.floor(mediaDuration / 100));
      int reportInterval = (onePercentInterval > 10 ? onePercentInterval : 10) * 1000;

      this.reportProgress("watching");
      handler.postDelayed(this, reportInterval /* ms */);
    }

    public void reportProgress(String playerState) {
      if (this.stopReportingEvents) {
        return;
      }

      int currentPosition = (int) (Math.floor(player.getCurrentPosition() / 1000));
      int mediaDuration = (int) (Math.floor(this.player.getContentDuration() / 1000));

      String videoId = this.videoId;

      WritableMap params = Arguments.createMap();
      params.putInt("progress", currentPosition);
      params.putInt("duration", mediaDuration);
      params.putString("videoId", videoId);
      params.putString("state", playerState);
      params.putBoolean("isLiveTV", isLiveTV);

      if (mediaDuration >= currentPosition) {
         CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerProgress", params);
      }

      if (playerState.equals("finished")) {
        this.stopReportingEvents = true;
      }
    }

    public void reportProgressForLiveTV(String action, String videoId) {
      this.videoId = videoId;
      this.reportProgress(action);
    }

    public void cancel() {
      System.out.println("here4 cancel ");

      this.handler.removeCallbacksAndMessages(null);
    }

  }

  private class PlayerEventListener implements Player.EventListener {

    private SimpleExoPlayer player;
    private String videoId;
    private Boolean isLiveTV;

    public PlayerEventListener(SimpleExoPlayer player, String videoId, Boolean isLiveTV) {
      this.player = player;
      this.videoId = videoId;
      this.isLiveTV = isLiveTV;
    }

    @Override
    public void onPlaybackStateChanged(@Player.State int playbackState) {
      if (playbackState == Player.STATE_ENDED) {
        showControls();
        WritableMap params = Arguments.createMap();
        WritableMap paramsEnded = Arguments.createMap();

        long currentPosition = player.getCurrentPosition();
        int currentPositionInteger = (int) (Math.floor(currentPosition / 1000));
        int mediaDuration = (int) (Math.floor(player.getContentDuration() / 1000));

        String videoId = this.videoId;
        params.putString("videoId", videoId);
        paramsEnded.putString("videoId", videoId);
        params.putInt("progress", currentPositionInteger);
        paramsEnded.putInt("progress", currentPositionInteger);
        params.putInt("duration", mediaDuration);
        paramsEnded.putInt("duration", mediaDuration);

        CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerProgress", params);
        CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerEnded", paramsEnded);
        finish();
      }
      updateButtonVisibility();
    }

    @Override
    public void onPlayerError(@NonNull ExoPlaybackException e) {
      long currentPosition = player.getCurrentPosition();
      int currentPositionInteger = (int) (Math.floor(currentPosition / 1000));
      int mediaDuration = (int) (Math.floor(player.getContentDuration() / 1000));

      float completionPercentage = (float) currentPositionInteger / (float) mediaDuration;

      Log.d("---", "1 onPlayerError/onPlayerEnded: " + currentPositionInteger);
      Log.d("---", "2 onPlayerError/onPlayerEnded: " + mediaDuration);
      Log.d("---", "3 onPlayerError/onPlayerEnded: " + completionPercentage * 100);
      Log.d("---", "4 onPlayerError/onPlayerEnded: isCompletionPercentageAboveThreshold" + (completionPercentage * 100 > 94));

      Log.d("---", "*1 onPlayerError/onPlayerEnded: " + player.getCurrentPosition());
      Log.d("---", "*2 onPlayerError/onPlayerEnded: " + player.getContentDuration());
      // Log.d("---", "*3 onPlayerError/onPlayerEnded: " + completionPercentage * 100);
      Log.d("---", "*4 onPlayerError/onPlayerEnded: isCompletionPercentageAboveThreshold" + (completionPercentage * 100 > 94));


      WritableMap paramsError = Arguments.createMap();
      paramsError.putString("videoId", videoId);
      paramsError.putString("error", e.toString());
      paramsError.putInt("progress", currentPositionInteger);
      paramsError.putInt("duration", mediaDuration);

      WritableMap paramsEnded = Arguments.createMap();
      paramsError.putString("videoId", videoId);
      paramsError.putString("error", e.toString());
      paramsError.putInt("progress", currentPositionInteger);
      paramsError.putInt("duration", mediaDuration);

      CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerError", paramsError);
      // if the ending of media has been reached OR completion is above a threshold aka 94% [will not work for shorter media below 30 seconds]
      if (currentPositionInteger == mediaDuration || (completionPercentage * 100 > 94)) {
        CustomExoplayerModule.getReactApplicationContextFromExoplayerBridgeModule().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("reportPlayerEnded", paramsEnded);
        finish();
      }

      if (isBehindLiveWindow(e)) {
        clearStartPosition();
        initializePlayer();
      } else {
        updateButtonVisibility();
        showControls();
      }
    }

    @Override
    @SuppressWarnings("ReferenceEquality")
    public void onTracksChanged(
        @NonNull TrackGroupArray trackGroups, @NonNull TrackSelectionArray trackSelections) {
      updateButtonVisibility();
      if (trackGroups != lastSeenTrackGroupArray) {
        MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        if (mappedTrackInfo != null) {
          if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
              == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
            showToast(R.string.error_unsupported_video);
          }
          if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO)
              == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
            showToast(R.string.error_unsupported_audio);
          }
        }
        lastSeenTrackGroupArray = trackGroups;
      }
    }
  }

  private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {

    @Override
    @NonNull
    public Pair<Integer, String> getErrorMessage(@NonNull ExoPlaybackException e) {
      String errorString = getString(R.string.error_generic);
      if (e.type == ExoPlaybackException.TYPE_RENDERER) {
        Exception cause = e.getRendererException();
        if (cause instanceof DecoderInitializationException) {
          // Special case for decoder initialization failures.
          DecoderInitializationException decoderInitializationException =
              (DecoderInitializationException) cause;
          if (decoderInitializationException.codecInfo == null) {
            if (decoderInitializationException.getCause() instanceof DecoderQueryException) {
              errorString = getString(R.string.error_querying_decoders);
            } else if (decoderInitializationException.secureDecoderRequired) {
              errorString =
                  getString(
                      R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
            } else {
              errorString =
                  getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
            }
          } else {
            errorString =
                getString(
                    R.string.error_instantiating_decoder,
                    decoderInitializationException.codecInfo.name);
          }
        }
      }
      return Pair.create(0, errorString);
    }
  }

  private static List<MediaItem> createMediaItems(Intent intent, DownloadTracker downloadTracker) {
    List<MediaItem> mediaItems = new ArrayList<>();
    for (MediaItem item : IntentUtil.createMediaItemsFromIntent(intent)) {
      @Nullable
      DownloadRequest downloadRequest =
          downloadTracker.getDownloadRequest(checkNotNull(item.playbackProperties).uri);
      if (downloadRequest != null) {
        MediaItem.Builder builder = item.buildUpon();
        builder
            .setMediaId(downloadRequest.id)
            .setUri(downloadRequest.uri)
            .setCustomCacheKey(downloadRequest.customCacheKey)
            .setMimeType(downloadRequest.mimeType)
            .setStreamKeys(downloadRequest.streamKeys)
            .setDrmKeySetId(downloadRequest.keySetId);
        mediaItems.add(builder.build());
      } else {
        mediaItems.add(item);
      }
    }
    return mediaItems;
  }
}
