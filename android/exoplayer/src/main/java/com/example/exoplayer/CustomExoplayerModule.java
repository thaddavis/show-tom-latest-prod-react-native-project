package com.example.exoplayer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;

import static com.example.exoplayer.CustomExoplayerModuleHelper.ExtractSubtitles;

public class CustomExoplayerModule extends ReactContextBaseJavaModule {
    private static final String TAG = "CustomExoplayerModule";

    private static ReactApplicationContext reactContext;

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    int LAUNCH_EXOPLAYER_ACTIVITY = 1;

    CustomExoplayerModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "CustomExoplayerModule";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }

    @ReactMethod
    public void show(String message, int duration) {
        Toast.makeText(getReactApplicationContext(), message, duration).show();
    }

    @ReactMethod
    public void launchPlayerForLiveTV(String contentType, String videoId, ReadableArray numbers, ReadableArray ids,
                                      ReadableArray titles, ReadableArray uris, ReadableMap subtitles, Integer currentIndex, boolean hideControls,
                                      Callback errorCallback, Callback successCallback)     {

        System.out.println("contentType" + contentType);
        System.out.println("videoId" + videoId);
        System.out.println("ids" + ids);
        System.out.println("here currentIndex " + currentIndex);

        String[] uriList = new String[numbers.size()];
        String[] idList = new String[numbers.size()];
        String[] titleList = new String[numbers.size()];
        String[] numberList = new String[numbers.size()];

        for (int i = 0; i < numbers.size(); i++) {
            uriList[i] = uris.getString(i);
            idList[i] = ids.getString(i);
            titleList[i] = titles.getString(i);
            numberList[i] = numbers.getString(i);
            System.out.println("here uriList[i] " + uriList[i]);
            System.out.println("here idList[i] " + idList[i]);
            System.out.println("here titleList[i] " + titleList[i]);
            System.out.println("here numberList[i] " + numberList[i]);
        }

        System.out.println("here uriList " + uriList);
        System.out.println("here idList " + idList);
        System.out.println("here titleList " + titleList);
        System.out.println("here numberList " + numberList);

        // --- Subtitles passed to PlayerActivity
        ReadableMapKeySetIterator itr = subtitles.keySetIterator();
        HashMap<String, String> subtitlesHashMap = new HashMap<String, String>();
        while (itr.hasNextKey()) {
            String K = itr.nextKey().toString();
            String V = subtitles.getString(K);
            subtitlesHashMap.put(K, V);
        }

        Uri contentUri;

        // For Video Progress
        contentUri = Uri.parse(uriList[0]);

        // To Build Array of Hash Map of CC tracks that line up with URI list
        HashMap<String, String>[] ccTracksArrayOfHashMaps = new HashMap[uris.size()]; // amount of episodes

        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            errorCallback.invoke(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }


        System.out.println("here live tv mode");
        String enableControls = hideControls ? "YES" : "NO";


        try {
            Intent exoPlayer2Intent = new Intent(currentActivity, com.example.exoplayer.PlayerActivity.class)
                    .setAction(com.example.exoplayer.IntentUtil.ACTION_VIEW).setData(contentUri)
                    .putExtra(com.example.exoplayer.IntentUtil.CONTENT_TYPE, contentType)
                    .putExtra(com.example.exoplayer.IntentUtil.SUBTITLES_EXTRA, subtitlesHashMap)
                    // .putExtra(com.example.exoplayer.IntentUtil.PROGRESS_IN_SECONDS, 0)
                    .putExtra(com.example.exoplayer.IntentUtil.HIDE_CONTROLS, enableControls)
                    .putExtra(com.example.exoplayer.IntentUtil.TITLE, titleList[currentIndex])
                    .putExtra(com.example.exoplayer.IntentUtil.VIDEO_ID, idList[currentIndex])
                    .putExtra(com.example.exoplayer.IntentUtil.URI_LIST_EXTRA, uriList)
                    .putExtra(com.example.exoplayer.IntentUtil.ID_LIST_EXTRA, idList)
                    .putExtra(com.example.exoplayer.IntentUtil.CC_LIST_EXTRA, ccTracksArrayOfHashMaps)
                    .putExtra(com.example.exoplayer.IntentUtil.TITLE_LIST, titleList)
                    .putExtra(com.example.exoplayer.IntentUtil.PLAYLIST_START_INDEX, currentIndex)
                    .putExtra(com.example.exoplayer.IntentUtil.NUMBER_LIST, numberList);

            currentActivity.startActivity(exoPlayer2Intent);
        } catch (Exception e) {
            errorCallback.invoke(e.toString());
        }
    }


    @ReactMethod
    public void launchPlayer(String contentType, String contentUriParam, String portalIdParam,
                             String licenseServerUrlParam, String title, String videoId, ReadableMap subtitles, Integer savedProgressInSeconds,
                             boolean isEncrypted, boolean hideControls, Callback errorCallback, Callback successCallback) {

        Log.d(TAG, "launchPlayer: ");
        Log.d(TAG, "videoId: " + videoId);
        String enableControls = hideControls ? "NO" : "YES";

        Intent exoPlayerIntent;
        Uri contentUri;
        String portalId;
        String licenseServerUrl;
        HashMap<String, String> subtitlesHashMap = ExtractSubtitles(subtitles);

        try {
            contentUri = Uri.parse(contentUriParam);
            // contentUri = Uri.parse("http://remote-xicms-exm-svc.xs.ocean.com:1935/direct/v2/video/92C8BF1B5F6E48A2815E5E79EE9B4817/F678384E661ACC3F2A7E612179E19242/0061537WS/A61537_E2_WS_XX_XX.mpd");
            portalId = portalIdParam;
            licenseServerUrl = licenseServerUrlParam;
            // licenseServerUrl = "https://remote-xicms-exm-svc.xs.ocean.com/keyserver/proxy";

            Activity currentActivity = getCurrentActivity();
            if (currentActivity == null) { /* errorCallback.invoke(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist"); */ return; }

            System.out.println("############# savedProgressInSeconds " + savedProgressInSeconds);
            // String[] keyRequestPropertiesArray = {"SwankPortal", "3A7ED520-9CBE-4573-8252-E4E73912F06A", "Host", "swankmp.net"};
            String[] keyRequestPropertiesArray = { "SwankPortal", portalId, "Host", "ocean.swankmp.net" };

            System.out.println("TRY TO START PLAYER ACTIVITY in SINGLE MEDIA MODE");
            System.out.println("############# contentUriParam " + contentUriParam);
            System.out.println("############# portalId " + portalId);
            System.out.println("############# licenseServerUrl " + licenseServerUrl);
            System.out.println("############# keyRequestProperties " + keyRequestPropertiesArray);
            System.out.println("############# subtitlesHashMap " + subtitlesHashMap);
            System.out.println("############# title " + title);
            System.out.println("############# videoId " + videoId);
            System.out.println("############# savedProgressInSeconds " + savedProgressInSeconds);
            System.out.println("############# enableControls " + enableControls);

            if (isEncrypted) {
                    exoPlayerIntent = new Intent(currentActivity, com.example.exoplayer.PlayerActivity.class)
                    .setAction(com.example.exoplayer.IntentUtil.ACTION_VIEW)
                    .setData(contentUri)
                    .putExtra(com.example.exoplayer.IntentUtil.DRM_SCHEME_EXTRA, "widevine")
                    .putExtra(com.example.exoplayer.IntentUtil.DRM_LICENSE_URI_EXTRA, licenseServerUrl)
                    .putExtra(com.example.exoplayer.IntentUtil.DRM_KEY_REQUEST_PROPERTIES_EXTRA, keyRequestPropertiesArray)
                    // .putExtra(com.example.exoplayer.IntentUtil.SUBTITLES_EXTRA, subtitlesHashMap)
                    .putExtra(com.example.exoplayer.IntentUtil.TITLE, title)
                    .putExtra(com.example.exoplayer.IntentUtil.VIDEO_ID, videoId)
                    .putExtra(com.example.exoplayer.IntentUtil.PROGRESS_IN_SECONDS, savedProgressInSeconds)
                    .putExtra(com.example.exoplayer.IntentUtil.HIDE_CONTROLS, enableControls)
                    .putExtra(com.example.exoplayer.IntentUtil.CONTENT_TYPE, contentType);
//                exoPlayerIntent = new Intent(currentActivity, com.example.exoplayer.PlayerActivity.class)
//                        .setAction(com.example.exoplayer.IntentUtil.ACTION_VIEW).setData(Uri.parse("https://ssdemo04.swankmp.com/ondemand/2.0/moviesSL/cenc/A49546_E2_WS_XX_XX/52594193/A49546_E2_WS_XX_XX.mpd"))
//                        .putExtra(com.example.exoplayer.IntentUtil.DRM_SCHEME_EXTRA, "widevine")
//                        .putExtra(com.example.exoplayer.IntentUtil.DRM_LICENSE_URI_EXTRA, "https://wvlsmod.swankmp.net/moddrm_proxy/proxy.py")
//                        .putExtra(com.example.exoplayer.IntentUtil.DRM_KEY_REQUEST_PROPERTIES_EXTRA, keyRequestPropertiesArray)
//                        .putExtra(com.example.exoplayer.IntentUtil.CONTENT_TYPE, contentType)
//                        .putExtra(com.example.exoplayer.IntentUtil.HIDE_CONTROLS, enableControls);
            } else {
                exoPlayerIntent = new Intent(currentActivity, com.example.exoplayer.PlayerActivity.class)
                        .setAction(com.example.exoplayer.IntentUtil.ACTION_VIEW)
                        .setData(contentUri)
                        .putExtra(com.example.exoplayer.IntentUtil.SUBTITLES_EXTRA, subtitlesHashMap)
                        .putExtra(com.example.exoplayer.IntentUtil.TITLE, title)
                        .putExtra(com.example.exoplayer.IntentUtil.VIDEO_ID, videoId)
                        .putExtra(com.example.exoplayer.IntentUtil.PROGRESS_IN_SECONDS, savedProgressInSeconds)
                        .putExtra(com.example.exoplayer.IntentUtil.HIDE_CONTROLS, enableControls)
                        .putExtra(com.example.exoplayer.IntentUtil.CONTENT_TYPE, contentType);
            }
            currentActivity.startActivityForResult(exoPlayerIntent, LAUNCH_EXOPLAYER_ACTIVITY);
        } catch (Exception e) {
            Log.d(TAG, "launchPlayer: " + e.toString());
        }
    }

    public static ReactApplicationContext getReactApplicationContextFromExoplayerBridgeModule() {
        return reactContext;
    }
}